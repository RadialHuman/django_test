https://www.youtube.com/watch?v=H5aVejBcM8k&list=PLB5jA40tNf3vkj5O2NCwMbDZo0Q8GV5Fn

# django_test

## TO create the basic setup
1. > django-admin startproject test1
1. > cd test1

## Create an app
1. > python manage.py startapp app1
1. type in models in models.py of app1
1. update settings.py for the new app in test1
1. > python manage.py makemigrations app1
1. > python manage.py migrate

## to create superuser
1. > python manage.py createsuperuser
1. enter admin, 1 email address, admin password

## run the server
1. > python manage.py runserver
1. => http://127.0.0.1:8000/admin/

## Populate the db
1. register in admin.py in app1
1. => http://127.0.0.1:8000/admin/
1. This will show the new app with Questions and choices
1. Populate using the GUI and connect it to respective questions as foreign key
