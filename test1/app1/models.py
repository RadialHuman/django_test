from django.db import models

# Create your models (things that go into the website) here.

# For polling there has to be questions
class Question(models.Model):
    question_text = models.CharField(max_length = 100)
    pub_date = models.DateTimeField('date published')

    # for making print in a better manner than just an object when called
    def __str__(self):
        return self.question_text

# For answers to that particular question model
class Choice(models.Model):
    choice_text = models.CharField(max_length = 200)
    votes = models.IntegerField(default = 0)
    # to make it link to the Question class
    question = models.ForeignKey(Question, on_delete= models.CASCADE)

    # for making print in a better manner than just an object when called
    def __str__(self):
        return self.choice_text 